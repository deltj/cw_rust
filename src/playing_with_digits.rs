/// Given integers n and p, find the sum of the digits of n taken to the successive
/// powers of p
pub fn psum(n: i64, p: i32) -> i64 {
    //  Find the digits of n
    let mut digit_vec: Vec<i64> = Vec::new();
    let mut tmp_n: i64 = n;
    while tmp_n > 0 {
        let digit = tmp_n % 10;
        println!("digit={0}", digit);
        digit_vec.push(digit);
        tmp_n /= 10;
    }
    digit_vec.reverse();

    //  Find the sum of the digits of n taken to the successive powers of p
    let mut psum: i64 = 0;
    for i in 0..digit_vec.len() {
        let digit: i64 = *digit_vec.get(i).unwrap();
        let power: u32 = (p as u32) + (i as u32);
        println!("digit={0}, power={1}", digit, power);
        psum += i64::pow(digit, power);
    }

    return psum;
}

/// Given a positive integer n written as abcd (a, b, c, and d being digits)
/// and a positive integer p, find a positive integer k, if it exists, such
/// that the sum of the digits of n taken to the successive powers of p is equal to k * n.
pub fn dig_pow(n: i64, p: i32) -> i64 {
    let psum: i64 = psum(n, p);
    let k_max: i64 = 32768;  //  A somewhat arbitrary upper bound for k

    for k in 1..=k_max {
        if psum == k * n {
            return k;
        }
    }

    return -1;
}

#[cfg(test)]
mod tests {
    use crate::playing_with_digits::*;

    #[test]
    fn psum_test1() {
        assert_eq!(288, psum(1234, 1));
        assert_eq!(89, psum(89, 1));
        assert_eq!(2360688, psum(46288, 3));
        assert_eq!(25, psum(5, 2));
    }

    #[test]
    fn dig_pow_test() {
        assert_eq!(1, dig_pow(89, 1));
        assert_eq!(-1, dig_pow(91, 1));
        assert_eq!(51, dig_pow(46288, 3));
        assert_eq!(5, dig_pow(5, 2));
        assert_eq!(12933, dig_pow(10383, 6));
    }
}