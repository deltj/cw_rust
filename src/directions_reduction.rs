#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Direction {
    North,
    East,
    West,
    South,
}

/// Perform a single-pass simplification of the specified directions.
/// Returns true if the pass resulted in a simplification, false if the
/// directions could not be further simplified.
pub fn dir_reduce(dir: &mut Vec<Direction>) -> bool {
    let reducible_patterns = [
        [Direction::North, Direction::South],
        [Direction::South, Direction::North],
        [Direction::East, Direction::West],
        [Direction::West, Direction::East],
    ];

    if dir.len() < 2 {
        return false;
    }

    for idx in 0..dir.len() - 1 {
        let s = &dir[idx..=idx+1];
        for rp in reducible_patterns {
            if s == rp {
                //  The slice [idx..=idx+1] is a reducible pattern, so it can
                //  be removed from the directions
                dir.remove(idx);
                dir.remove(idx);
                return true;
            }
        }
    }

    return false;
}

/// Simplify the specified directions
pub fn dir_reduc(dir: &[Direction]) -> Vec<Direction> {
    println!("{:?}", dir);

    let mut result = dir.to_vec();

    let mut simplified = true;
    while simplified {
        simplified = dir_reduce(&mut result);
    }

    return result;
}

#[cfg(test)]
mod tests {
    use super::{*, Direction::*};

    #[test]
    fn dir_reduce_test1() {
        let mut dv = [North, South].to_vec();
        let result = dir_reduce(&mut dv);
        assert_eq!(true, result);
        assert_eq!(0, dv.len());
    }

    #[test]
    fn dir_reduce_test2() {
        let mut dv = [North, North, South].to_vec();
        let result = dir_reduce(&mut dv);
        assert_eq!(true, result);
        assert_eq!([North].to_vec(), dv);
    }

    #[test]
    fn dir_reduc_test() {
        let d1 = [North, South, South, East, West, North, West];
        assert_eq!(dir_reduc(&d1), [West]);

        let d2 = [North, South, East, West];
        assert_eq!(dir_reduc(&d2), []);

        let d3 = [North, East, West, South, West, West];
        assert_eq!(dir_reduc(&d3), [West, West]);

        let d4 = [North, West, South, East];
        assert_eq!(dir_reduc(&d4), [North, West, South, East]);
    }
}