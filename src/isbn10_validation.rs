use regex::Regex;

pub fn valid_isbn10(isbn: &str) -> bool {
    //  A valid ISBN-10 is 10 characters long.  Before doing a relatively
    //  expensive regex match, make sure the input length is correct
    if isbn.len() != 10 {
        return false;
    }

    //  Perform regex validation to make sure this at least kind of looks
    //  like an ISBN-10
    let re = Regex::new(r"^[0-9]{9}[0-9|X]$").unwrap();
    if !re.is_match(isbn) {
        return false;
    }

    //  An ISBN-10 number is valid if the sum of the digits multiplied by
    //  their position modulo 11 equals zero
    let digits: Vec<_> = isbn.chars().map(|c| match c {
            '1'..='9' => c.to_digit(10).unwrap(),
            'X' => 10,
            _ => 0,
        }).collect();

    let mut sum = 0;
    for i in 0..10 {
        sum += digits[i] * ((i as u32) + 1);
    }

    if sum % 11 != 0 {
        return false;
    }

    return true;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn valid_isbn10_test() {
        assert_eq!(false, valid_isbn10("definitely not an ISBN"));
        assert_eq!(true,  valid_isbn10("1112223339"));
        assert_eq!(false, valid_isbn10("111222333"));
        assert_eq!(false, valid_isbn10("1112223339X"));
        assert_eq!(true,  valid_isbn10("1234554321"));
        assert_eq!(false, valid_isbn10("1234512345"));
        assert_eq!(true,  valid_isbn10("048665088X"));
        assert_eq!(false, valid_isbn10("X123456788"));
    }
}