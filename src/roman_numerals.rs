/// Look up the decimal value for a single Roman Numeral digit
fn romap(c: char) -> u64 {
    match c {
        'I' => 1,
        'V' => 5,
        'X' => 10,
        'L' => 50,
        'C' => 100,
        'D' => 500,
        'M' => 1000,
        _ => 0,
    }
}

/// Convenience function to return the roman numeral at some index, or zero
/// if the index is invalid
fn get_romnum_at_index(roman: &str, index: usize) -> u64 {
    if index < roman.len() {
        return romap(roman.chars().nth(index).unwrap());
    } else {
        return 0;
    }
}

/// Return the decimal value for the given roman numeral string
/// Solution adapted from: https://stackoverflow.com/a/26667855
pub fn roman_as_num(roman: &str) -> u64 {
    let mut num: u64 = 1000;
    for i in 0..roman.len() {
        let r0 = get_romnum_at_index(roman, i);
        let r1 = get_romnum_at_index(roman, i + 1);
        if r0 < r1 {
            num -= r0;
        } else {
            num += r0;
        }
    }

    return num - 1000;
}

#[cfg(test)]
mod tests {
    use crate::roman_numerals::*;

    #[test]
    fn roman_as_num_test1() {
        assert_eq!(1, roman_as_num("I"));
        assert_eq!(5, roman_as_num("V"));
        assert_eq!(10, roman_as_num("X"));
        assert_eq!(50, roman_as_num("L"));
        assert_eq!(100, roman_as_num("C"));
        assert_eq!(500, roman_as_num("D"));
        assert_eq!(1000, roman_as_num("M"));
    }

    #[test]
    fn roman_as_num_test2() {
        assert_eq!(3, roman_as_num("III"));
        assert_eq!(4, roman_as_num("IV"));
        assert_eq!(9, roman_as_num("IX"));
        assert_eq!(40, roman_as_num("XL"));
        assert_eq!(90, roman_as_num("XC"));
        assert_eq!(400, roman_as_num("CD"));
        assert_eq!(900, roman_as_num("CM"));
    }
}