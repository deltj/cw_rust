pub mod bounce;
pub mod playing_with_digits;
pub mod roman_numerals;
pub mod directions_reduction;
pub mod isbn10_validation;