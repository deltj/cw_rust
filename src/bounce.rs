/// Given an initial height, bounce factor, and a height of interest,
/// determine how many times a bouncing ball will pass the height of interest    
pub fn height_pass_count(h0: f64, b: f64, hi: f64) -> i32 {
    
    //  Initial height must be greater than zero
    if h0 <= 0.0 {
        return -1;
    }

    //  Bounce factor must be between zero and one
    if b <= 0.0 || b >= 1.0 {
        return -1;
    }

    //  Height of interest must be less than initial height
    if hi >= h0 {
        return -1;
    }

    //  C is the count of how many times the ball has passed the point of interest
    let mut c: i32 = 1;

    let mut h: f64  = h0;
    while h > hi {
        h = h * b;

        if h > hi {
            c += 2;
        }
    }

    return c;
}

#[cfg(test)]
mod tests {
    use crate::bounce::*;
    
    #[test]
    fn bounce_test_invalid_initial_height() {
        let c: i32 = height_pass_count(0.0, 0.6, 10.0);
        assert_eq!(-1, c);
    }

    #[test]
    fn bounce_test_invalid_bounce_1() {
        let c: i32 = height_pass_count(10.0, 0.0, 10.0);
        assert_eq!(-1, c);
    }

    #[test]
    fn bounce_test_invalid_bounce_2() {
        let c: i32 = height_pass_count(10.0, 1.0, 9.0);
        assert_eq!(-1, c);
    }

    #[test]
    fn bounce_test_invalid_height_of_interest() {
        let c: i32 = height_pass_count(10.0, 0.0, 10.1);
        assert_eq!(-1, c);
    }

    #[test]
    fn bounce_test1() {
        let c = height_pass_count(3.0, 0.66, 1.5);
        assert_eq!(3, c);
    }

    #[test]
    fn bounce_test2() {
        let c: i32 = height_pass_count(30.0, 0.66, 1.5);
        assert_eq!(15, c);
    }

    #[test]
    fn bounce_test3() {
        let c: i32 = height_pass_count(40.0, 0.4, 10.0);
        assert_eq!(3, c);
    }

}